#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include "dialogcorrect.h"
#include "dbsetup.h"
#include "filesfactory.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QDebug>
#include "databasecommunication.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class DialogCorrect;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    //QByteArray Hash_Password(QString string);
    //int Get_User_Id();

private slots:
    void on_pushButton_login_clicked();
    void on_pushButton_signin_clicked();
    void loggedout();

signals:
   void credentials(QString login, QString password);

private:
    Ui::MainWindow *ui;
    DialogCorrect *dialogCorrect;
    void createFileExplorer();
};
#endif // MAINWINDOW_H

