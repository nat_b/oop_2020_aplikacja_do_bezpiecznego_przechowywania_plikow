#ifndef DIALOGCORRECT_H
#define DIALOGCORRECT_H

#include <QDialog>
#include <QtCore>
#include <QtGui>
#include <QFileSystemModel>
#include <QFileDialog>
#include "addfiles.h"
#include "filesfactory.h"
#include "viewfile.h"
#include <QSplitter>
#include <QDirModel>
#include <QListView>
#include <QDebug>
#include <QTreeView>
#include "ui_dialogcorrect.h"
#include "databasecommunication.h"



namespace Ui {
class DialogCorrect;
}

class DialogCorrect : public QDialog
{
    Q_OBJECT
    QFileDialog file_dialog{this};

public:
    explicit DialogCorrect(QWidget *parent = nullptr);
    ~DialogCorrect();

private slots:
    void on_pushButton_logout_clicked();

   // void on_pushButton_del_2_clicked();
    void getCredentials(QString login, QString password);

    void on_pushButton_add_clicked();

    void on_pushButton_del_clicked();
    void on_pushButton_open_clicked();

signals:
    void logout();

private:
    Ui::DialogCorrect *ui;
    QFileSystemModel *dirmodel;
    QFileSystemModel *filemodel;
    QString login_;
    QString password_;
    ViewFile *viewfile;

};

#endif // DIALOGCORRECT_H
