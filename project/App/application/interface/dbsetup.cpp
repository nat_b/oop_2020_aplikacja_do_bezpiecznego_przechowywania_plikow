#include "dbsetup.h"

DbSetup::DbSetup(QObject *parent) : QObject(parent)
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    QString path = QDir::currentPath() + "sqlite.db";
    db.setDatabaseName(path);
}

bool DbSetup::CreateConnection()
{
    if(!db.open())
    {
        return false;
    }

   QSqlQuery query;
   query.exec("PRAGMA foreign_keys = ON");
   query.exec("CREATE TABLE users("
              "id INTEGER NOT NULL PRIMARY KEY,"
              "login TEXT NOT NULL,"
              "password TEXT NOT NULL)");
   query.exec("CREATE TABLE files("
              "id INTEGER NOT NULL PRIMARY KEY,"
              "name TEXT NOT NULL,"
              "content BLOB NOT NULL,"
              "user_id INTEGER NOT NULL,"
              "type TEXT NOT NULL,"
              "FOREIGN KEY(user_id)"
              "REFERENCES users(id))");

   return true;
}

