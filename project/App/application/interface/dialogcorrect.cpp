#include "dialogcorrect.h"


DialogCorrect::DialogCorrect(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogCorrect)
{
    ui->setupUi(this);
    QString sPath = QDir::currentPath();
    QString dirName = "user";
    QDir().mkdir(dirName);
    sPath = sPath + "/" + dirName;

    dirmodel = new QFileSystemModel(this);
    dirmodel->setFilter(QDir::NoDotAndDotDot | QDir::AllDirs | QDir::Files );
    dirmodel->setRootPath(sPath);

    QTreeView v;
    v.setModel(dirmodel);
    ui->treeView->setModel(dirmodel);

    QModelIndex idx = dirmodel->index(0,0);
    QString name = idx.model()->data(idx, Qt::DisplayRole).toString();

     while(name != dirName)
    {
        idx = dirmodel->index(0,0,idx);
        name = idx.model()->data(idx, Qt::DisplayRole).toString();
    }

    v.setRootIndex(idx);
    ui->treeView->setRootIndex(idx);
    v.show();
}

DialogCorrect::~DialogCorrect()
{
    delete ui;
}


void DialogCorrect::on_pushButton_logout_clicked()
{
    hide();
    emit logout();
}

void DialogCorrect::on_pushButton_add_clicked()
{
    QSqlDatabase::database();
    DatabaseCommunication dbComm;
    AddFiles addFls;
    FilesFactory f_factory;
    int id = dbComm.Get_User_Id(login_, password_);
    QString name = QFileDialog::getOpenFileName(this);
    addFls.AddToDatabase(id,name);
    f_factory.makeTrueUserFiles(id);
}

void DialogCorrect::on_pushButton_del_clicked()
{
    QSqlDatabase::database();
    DatabaseCommunication dbComm;
    AddFiles addFls;
    FilesFactory f_factory;
    int id = dbComm.Get_User_Id(login_, password_);
    QModelIndex index = ui->treeView->currentIndex();
    if(!index.isValid()) return;

    if(dirmodel->fileInfo(index).isFile())
    {
        if(id)
        {
            addFls.DeleteRegisteredUserFile(id, dirmodel->fileInfo(index).baseName(), dirmodel->fileInfo(index).suffix());
            f_factory.clearUserDir();
            f_factory.createUserDir();
        }
        else
        {
            addFls.DeleteFakeUserFile(dirmodel->fileInfo(index).baseName(), dirmodel->fileInfo(index).suffix());
        }
       f_factory.makeTrueUserFiles(id);
    }
}

 void DialogCorrect::getCredentials(QString login, QString password)
 {
     this->login_ = login;
     this->password_ = password;
 }

void DialogCorrect::on_pushButton_open_clicked()
{
    QModelIndex idx = ui->treeView->currentIndex();
    QString path_file;

    path_file = dirmodel->fileInfo(idx).filePath();

    viewfile = new ViewFile(this);
    viewfile->show();
    viewfile->view_file(path_file);
}
