#include "filesfactory.h"



FilesFactory::FilesFactory(QObject *parent) : QObject(parent)
{

}


void FilesFactory::clearUserDir(QString dirPath)
{
    QDir dir(dirPath);
    if(dir.exists())
    {
        dir.removeRecursively();
    }
}


void FilesFactory::createUserDir(QString dirPath)
{
    if(!QDir().mkdir(dirPath))
    {
        qDebug() << "directory create error";
        return;
    }
}


void FilesFactory::createFile(QString file_name, QString content, QString type, QString dirPath)
{
    QFile file(dirPath + "/user/" + file_name + type);
    if (file.open(QIODevice::ReadWrite))
    {
        QTextStream out(&file);
        out << content;
        file.close();
    }
    else
    {
        qDebug() << "file open error";
        return;
    }
}


void FilesFactory::makeTrueUserFiles(int user_id)
{
    QSqlDatabase::database();
    QSqlQueryModel model;
    QString user = QString::number(user_id);
    model.setQuery("SELECT name, type, content FROM files WHERE user_id ='" + user + "'");

    if( model.rowCount()>0 )
    {
            for (int i = 0; i < model.rowCount(); ++i)
            {
                QString name = model.record(i).value("name").toString();
                QString content = model.record(i).value("content").toString();
                QString type = model.record(i).value("type").toString();
                createFile(name, content, type);
            }
     }
}


void FilesFactory::copyFakeFiles()
{
    QDir currDir = QDir::current();
    for(int i=0; i<4; ++i)
    {
        currDir.cdUp();
    }

    QString rootDir = currDir.path();

    QDir directory(rootDir);
    QStringList files = directory.entryList(QStringList(),QDir::Files);

    for(QString filename : files)
    {
        QString filePath = rootDir + '/' + filename;
        QString userPath = QDir::currentPath() + "/user" + '/' + filename;
        QFile::copy(filePath, userPath);

        QString filecontent;
        QFile file(filePath);
        if (file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream in(&file);
            QString filecontent = in.readAll();

            QFile fileout(userPath);
            if (fileout.open(QIODevice::WriteOnly | QIODevice::Text))
            {
                QString content;
                Encryptor enc;
                content=enc.encrypt(filecontent);
                QTextStream out(&fileout);
                out << content;
                fileout.close();
            }
            else
                qDebug() << "file open error";

            file.close();
        }
        else
            qDebug() << "file open error";
    }
}











