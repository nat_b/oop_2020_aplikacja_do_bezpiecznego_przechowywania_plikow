#ifndef VIEWFILE_H
#define VIEWFILE_H

#include <QDialog>
#include <QFile>
#include <QDirModel>
#include <QDebug>
#include "ui_viewfile.h"
#include "encryptor.h"

namespace Ui {
class ViewFile;
}

class ViewFile : public QDialog
{
    Q_OBJECT

public:
    explicit ViewFile(QWidget *parent = nullptr);
    ~ViewFile();
    void view_file(QString file_path);

private:
    Ui::ViewFile *ui;
};

#endif // VIEWFILE_H
