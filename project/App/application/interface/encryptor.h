#ifndef ENCRYPTOR_H
#define ENCRYPTOR_H

#include <QObject>
#include<iostream>
#include<fstream>
#include<stdio.h>
#include <QDebug>

class Encryptor : public QObject
{
    Q_OBJECT
public:
    explicit Encryptor(QObject *parent = nullptr);
    QString encrypt(QString str2encrypt);
    QString decrypt(QString str2decrypt);
};

#endif // ENCRYPTOR_H
