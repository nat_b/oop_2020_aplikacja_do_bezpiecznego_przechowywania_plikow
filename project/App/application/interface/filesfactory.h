#ifndef FILESFACTORY_H
#define FILESFACTORY_H

#include <QObject>
#include <QFile>
#include <vector>
#include <QDirModel>
#include <QDebug>
#include "dbsetup.h"
#include "encryptor.h"

class FilesFactory : public QObject
{
    Q_OBJECT
public:
    explicit FilesFactory(QObject *parent = nullptr);
    void clearUserDir(QString dirPath = QDir::currentPath() + "/user");
    void createUserDir(QString dirPath = QDir::currentPath() + "/user");
    void makeTrueUserFiles(int user_id);
    void createFile(QString file_name, QString content, QString type=".txt", QString dirPath = QDir::currentPath());
    void copyFakeFiles();

signals:

};

#endif // FILESFACTORY_H
